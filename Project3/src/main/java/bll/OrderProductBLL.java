package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.OrderProductDAO;
import model.OrderProduct;

public class OrderProductBLL {
	
	private OrderProductDAO orderProductDAO;
	
	public OrderProductBLL() {
		orderProductDAO = new OrderProductDAO();
	}
	
	public OrderProduct findById(int id) {
		OrderProduct orderProduct = orderProductDAO.findOrderProduct(id);
		if (orderProduct == null) {
			throw new NoSuchElementException("The orderProduct with id =" + id + " was not found!");
		}
		return orderProduct;
	}
	
	public void deleteById(int id) {
		orderProductDAO.deleteOrderProduct(id);
	}

	
	public void update(int id, OrderProduct p) {
		orderProductDAO.update(id, p);
	}
	
	public void insert(OrderProduct p) {
		orderProductDAO.insertOrderProduct(p);
	}
	
	public List<OrderProduct> findAll() {
		return orderProductDAO.findAllOrderProduct();
		
	}
}
