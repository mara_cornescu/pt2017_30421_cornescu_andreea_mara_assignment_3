package bll.validators;

import model.Product;

public class PriceValidator implements Validator<Product> {

	private static final int MIN_PRICE = 1;
	private static final int MAX_PRICE = 1000;
	
	public void validate(Product t) {
		if (t.getProductPrice() < MIN_PRICE || t.getProductPrice() > MAX_PRICE) {
			throw new IllegalArgumentException("The Product Price limit is not respected!");
		}
	}

}
