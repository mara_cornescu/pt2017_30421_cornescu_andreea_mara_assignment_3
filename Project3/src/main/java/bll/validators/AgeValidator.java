package bll.validators;

import model.Customer;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class AgeValidator implements Validator<Customer> {
	private static final int MIN_AGE = 7;
	private static final int MAX_AGE = 30;

	public void validate(Customer t) {

		if (t.getCustomerAge() < MIN_AGE || t.getCustomerAge() > MAX_AGE) {
			throw new IllegalArgumentException("The Customer Age limit is not respected!");
		}

	}

}