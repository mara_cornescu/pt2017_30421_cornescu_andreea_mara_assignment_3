package bll.validators;

import model.Product;

public class StockValidator implements Validator<Product> {

	private static final int MIN_STOCK = 0;
	private static final int MAX_STOCK = 200;
	
	public void validate(Product t) {
		if (t.getProductStock() < MIN_STOCK) {
			throw new IllegalArgumentException("Under-Stock!");
		}
		if(t.getProductStock() > MAX_STOCK) {
			throw new IllegalArgumentException("Over-Stock!");
		}
	}

}
