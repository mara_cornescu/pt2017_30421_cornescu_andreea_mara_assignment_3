package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.AgeValidator;
import bll.validators.Validator;
import dao.CustomerDAO;
import model.Customer;

public class CustomerBLL {

	private List<Validator<Customer>> validators;
	private CustomerDAO customerDAO;
	
	public CustomerBLL() {
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new AgeValidator());
		customerDAO = new CustomerDAO();
	}
	
	public Customer findById(int id) {
		Customer customer = customerDAO.findCustomer(id);
		if (customer == null) {
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return customer;
	}
	
	public void deleteById(int id) {
		customerDAO.deleteCustomer(id);
	}
	
	public void update(int id, Customer c) {
		for (Validator<Customer> v : validators) {
			v.validate(c);
		}
		customerDAO.updateCustomer(id, c);
	}
	
	public void insert(Customer c) {
		for (Validator<Customer> v : validators) {
			v.validate(c);
		}
		customerDAO.insertCustomer(c);
	}
	
	public List<Customer> findAll() {
		return customerDAO.findAllCustomer();
	}
} 
