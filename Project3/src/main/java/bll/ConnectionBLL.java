package bll;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import bll.OrderCustomerBLL;
import bll.OrderProductBLL;
import bll.ProductBLL;

import model.*;

public class ConnectionBLL {
	
	private ProductBLL productBLL;
	private OrderCustomerBLL orderCustomerBLL;
	private OrderProductBLL orderProductBLL;
	
	public ConnectionBLL() {
		productBLL = new ProductBLL();
		orderCustomerBLL = new OrderCustomerBLL();
		orderProductBLL = new OrderProductBLL();
	}
	
	
	public void connectTable(Customer c, Product p, int quantity) {
		
		boolean isAlreadyIn = false;
		OrderCustomer currentOrder = new OrderCustomer();
		
		int totalPrice = p.getProductPrice() * quantity;
		p.setProductStock(p.getProductStock() - quantity);
		
		Product product = new Product(p.getProductID(), p.getProductName(), p.getProductPrice(), p.getProductStock());
		productBLL.update(p.getProductID(), product);
		
		List<OrderCustomer> orderCustomerList  = orderCustomerBLL.findAll();
		
		for(OrderCustomer oc : orderCustomerList) {
			if(oc.getCustomerID() == c.getCustomerID()) {
				isAlreadyIn = true;
				currentOrder = oc;
				break;
			}	
		}
		
		if(isAlreadyIn == false) {
			OrderCustomer orderCustomer = new OrderCustomer(orderCustomerBLL.lastElement() + 1, c.getCustomerID(), totalPrice);
			orderCustomerBLL.insert(orderCustomer);
			
			OrderProduct orderProduct = new OrderProduct(orderCustomer.getOrderID(), p.getProductID(), quantity, totalPrice);
			orderProductBLL.insert(orderProduct);
		}
		else {
			OrderProduct orderProduct = new OrderProduct(currentOrder.getOrderID(), p.getProductID(), quantity, totalPrice);
			orderProductBLL.insert(orderProduct);
		}	
	}
	
	
	public void createBill(Customer c) {
		
		List<OrderCustomer> orderCustomerList  = orderCustomerBLL.findAll();
		List<OrderProduct> orderProductList  = orderProductBLL.findAll();
		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("bill.txt"), "utf-8"));
			writer.write("Client " + c.getCustomerName() + " bought: ");
	
			int totalPrice = 0;
			for(OrderCustomer oc : orderCustomerList) {
				if(oc.getCustomerID() == c.getCustomerID()) {
					int orderNumber = oc.getOrderID();
					for(OrderProduct op: orderProductList) {
						if(op.getOrderID() == orderNumber) {
							int productNumber = op.getProductID();
							Product productBought = productBLL.findById(productNumber);
							
							((BufferedWriter) writer).newLine();
							writer.write(productBought.getProductName() + " " + + op.getProductQuantity() + " x " + productBought.getProductPrice() + " ... " + op.getUnitPrice() +  " RON ");
							
							totalPrice += op.getUnitPrice();
					}
				}
				((BufferedWriter) writer).newLine();
				writer.write("TOTAL " + totalPrice + " RON ");
				break;
			}
		}
		} catch (IOException ex) {
			} finally {
			   try {
				   writer.close();
			   } catch (Exception ex) {		   
			   }
			}
	}
	
}
