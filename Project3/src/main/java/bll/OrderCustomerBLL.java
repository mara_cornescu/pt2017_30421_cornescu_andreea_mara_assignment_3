package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.OrderCustomerDAO;
import model.OrderCustomer;

public class OrderCustomerBLL {
	
	private OrderCustomerDAO orderCustomerDAO;
	
	public OrderCustomerBLL() {
		orderCustomerDAO = new OrderCustomerDAO();
	}
	
	public OrderCustomer findById(int id) {
		OrderCustomer orderCustomer = orderCustomerDAO.findOrderCustomer(id);
		if (orderCustomer == null) {
			throw new NoSuchElementException("The orderCustomer with id =" + id + " was not found!");
		}
		return orderCustomer;
	}
	
	public void deleteById(int id) {
		orderCustomerDAO.deleteOrderCustomer(id);
	}

	
	public void update(int id, OrderCustomer p) {
		orderCustomerDAO.update(id, p);
	}
	
	public void insert(OrderCustomer p) {
		orderCustomerDAO.insertOrderCustomer(p);
	}
	
	public List<OrderCustomer> findAll() {
		return orderCustomerDAO.findAllOrderCustomer();
		
	}
	
	public int lastElement() {
		int length;
		if(orderCustomerDAO.findAllOrderCustomer().size() == 0)
			return  0;
		
		length = orderCustomerDAO.findAllOrderCustomer().size() - 1;
		int i = orderCustomerDAO.findAllOrderCustomer().get(length).getOrderID();	
		
		return i;
	}
}
