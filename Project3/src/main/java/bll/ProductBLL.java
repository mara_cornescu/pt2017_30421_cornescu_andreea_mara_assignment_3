package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.PriceValidator;
import bll.validators.StockValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	
	private ProductDAO productDAO;
	private List<Validator<Product>> validators;
	
	public ProductBLL() {
		productDAO = new ProductDAO();
		validators = new ArrayList<Validator<Product>>();
		validators.add(new PriceValidator());
		validators.add(new StockValidator());
	}
	
	public Product findById(int id) {
		Product product = productDAO.findProduct(id);
		if (product == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return product;
	}
	
	public void deleteById(int id) {
		productDAO.deleteProduct(id);
	}

	
	public void update(int id, Product p) {
		for (Validator<Product> v : validators) {
			v.validate(p);
		}
		productDAO.updateProduct(id, p);
	}
	
	public void insert(Product p) {
		for (Validator<Product> v : validators) {
			v.validate(p);
		}
		productDAO.insertProduct(p);
	}
	
	public List<Product> findAll() {
		return productDAO.findAllProduct();
		
	}
	
}
