package model;

public class Product {

	private int productID;
	private String productName;
	private int productPrice;
	private int productStock;
	
	public Product() {
		
	}
	
	public Product(int productID, String productName, int productPrice, int productStock) {
		setProductID(productID);
		setProductName(productName);
		setProductPrice(productPrice);
		setProductStock(productStock);
	}
	
	public void setProductID(int productID) {
		this.productID = productID;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setProductStock(int productStock) {
		this.productStock = productStock;
	}
	
	public int getProductID() {
		return productID;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public int getProductPrice() {
		return productPrice;
	}
	
	public int getProductStock() {
		return productStock;
	}
	
	@Override
	public String toString() {
		return "Product [productId=" + productID + ", name=" + productName + ", price=" + productPrice +", stock=" + productStock + "]";
	}
	
	
	
}
