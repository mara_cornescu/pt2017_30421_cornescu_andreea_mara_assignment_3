package model;

public class Customer {
	
	private int customerID;
	private String customerName;
	private String customerAddress;
	private String customerEmail;
	private int customerAge;
	
	public Customer() {
		
	}
	
	public Customer(int customerID, String customerName, String customerAddress, String customerEmail, int customerAge) {
		setCustomerID(customerID);
		setCustomerName(customerName);
		setCustomerAddress(customerAddress);
		setCustomerEmail(customerEmail);
		setCustomerAge(customerAge);
	}
	
	public Customer(String customerName, String customerAddress, String customerEmail, int customerAge) {

		setCustomerName(customerName);
		setCustomerAddress(customerAddress);
		setCustomerEmail(customerEmail);
		setCustomerAge(customerAge);
	}
	
	
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}
	
	public int getCustomerID() {
		return customerID;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	
	public String getCustomerAddress() {
		return customerAddress;
	}
	
	public String getCustomerEmail() {
		return customerEmail;
	}
	
	public int getCustomerAge() {
		return customerAge;
	}
	
	@Override
	public String toString() {
		return "Customer [id=" + customerID + ", name=" + customerName + ", address=" + customerAddress + ", email=" + customerEmail + ", age=" 
				+ customerAge + "]";
	}
	
}
