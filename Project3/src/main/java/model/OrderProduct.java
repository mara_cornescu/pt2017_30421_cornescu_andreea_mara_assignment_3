package model;

public class OrderProduct {

	private int orderID;
	private int productID;
	private int productQuantity;
	private int unitPrice;
	
	public OrderProduct() {
		
	}
	
	public OrderProduct(int orderID, int productID, int productQuantity, int unitPrice) {
		setOrderID(orderID);
		setProductID(productID);
		setProductQuantity(productQuantity);
		setUnitPrice(unitPrice);
	}
	
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	
	public void setProductID(int productID) {
		this.productID = productID;
	}
	
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	public int getProductID() {
		return productID;
	}
	
	public int getUnitPrice() {
		return unitPrice;
	}
	
	public int getProductQuantity() {
		return productQuantity;
	}
	
	@Override
	public String toString() {
		return "OrderProduct [orderId=" + orderID + ", productID=" + productID + ", unitPrice=" + unitPrice + "]";
	}

}
