package model;

public class OrderCustomer {

	private int orderID;
	private int customerID;
	private int totalPrice;
	
	public OrderCustomer() {
		
	}
	 
	public OrderCustomer(int orderID, int customerID, int totalPrice) {
		setOrderID(orderID);
		setCustomerID(customerID);
		setTotalPrice(totalPrice);
	}
	
	public OrderCustomer(int customerID, int totalPrice) {
		setCustomerID(customerID);
		setTotalPrice(totalPrice);
	}
	
	public OrderCustomer(int customerID) {
		setOrderID(orderID);
		setCustomerID(customerID);
	}
	
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	public int getCustomerID() {
		return customerID;
	}
	
	public int getTotalPrice() {
		return totalPrice;
	}
	
	@Override
	public String toString() {
		return "Order [orderId=" + orderID + ", customerID=" + customerID + ", totalPrice=" + totalPrice + "]";
	}
	
	
	
}
