package start;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.CustomerBLL;
import bll.OrderCustomerBLL;
import bll.OrderProductBLL;
import bll.ProductBLL;
import bll.ConnectionBLL;
import model.Customer;
import model.OrderCustomer;
import model.Product;
import presentation.AbstractTable;
import presentation.Controller;
import presentation.View;
import presentation.ViewOrder;

public class Start {
	

	public static void main(String[] args) throws SQLException {
		
		View guiView = new View();
		//ViewOrder orderView = new ViewOrder();
		
		Controller controller = new Controller(guiView);
		

	}
}
