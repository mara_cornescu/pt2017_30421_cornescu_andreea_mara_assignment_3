package dao;

import java.util.List;

import model.OrderCustomer;

public class OrderCustomerDAO extends AbstractDAO<OrderCustomer> {
	
	public OrderCustomer findOrderCustomer(int id) {
		OrderCustomerDAO orderCustomer = new OrderCustomerDAO();
		return orderCustomer.findById(id);
	}
	
	public void deleteOrderCustomer(int id) {
		OrderCustomerDAO orderCustomer = new OrderCustomerDAO();
		orderCustomer.deleteById(id);
	}
	
	
	public void updateOrderCustomert(int id, OrderCustomer c) {
		OrderCustomerDAO orderCustomer = new OrderCustomerDAO();
		orderCustomer.update(id, c);
	}
	
	public void insertOrderCustomer(OrderCustomer p) {
		OrderCustomerDAO orderCustomer = new OrderCustomerDAO();
		orderCustomer.insert(p);
	}
	
	public List<OrderCustomer> findAllOrderCustomer() {
		OrderCustomerDAO orderCustomer = new OrderCustomerDAO();
		List<OrderCustomer> orderCustomers = orderCustomer.findAll();
		return orderCustomers;
	}
	
}
