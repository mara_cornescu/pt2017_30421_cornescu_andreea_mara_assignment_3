package dao;

import java.util.List;

import model.Customer;

public class CustomerDAO extends AbstractDAO<Customer>{

	public Customer findCustomer(int id) {
		CustomerDAO client = new CustomerDAO();
		return client.findById(id);
	}
	
	public void deleteCustomer(int id) {
		CustomerDAO client = new CustomerDAO();
		client.deleteById(id);
	}
	
	
	public void updateCustomer(int id, Customer c) {
		CustomerDAO client = new CustomerDAO();
		client.update(id, c);
	}
	
	public void insertCustomer(Customer c) {
		CustomerDAO client = new CustomerDAO();
		client.insert(c);
	}
	
	public List<Customer> findAllCustomer() {
		CustomerDAO client = new CustomerDAO();
		List<Customer> clients = client.findAll();
		return clients;
	}
}