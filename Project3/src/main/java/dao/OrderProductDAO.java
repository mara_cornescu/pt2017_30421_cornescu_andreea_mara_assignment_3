package dao;

import java.util.List;

import model.OrderProduct;

public class OrderProductDAO extends AbstractDAO<OrderProduct>{
	
	public OrderProduct findOrderProduct(int id) {
		OrderProductDAO orderProduct = new OrderProductDAO();
		return orderProduct.findById(id);
	}
	
	public void deleteOrderProduct(int id) {
		OrderProductDAO orderProduct = new OrderProductDAO();
		orderProduct.deleteById(id);
	}
	
	
	public void updateOrderCustomert(int id, OrderProduct p) {
		OrderProductDAO orderProduct = new OrderProductDAO();
		orderProduct.update(id, p);
	}
	
	public void insertOrderProduct(OrderProduct p) {
		OrderProductDAO orderProduct = new OrderProductDAO();
		orderProduct.insert(p);
	}
	
	public List<OrderProduct> findAllOrderProduct() {
		OrderProductDAO orderProduct = new OrderProductDAO();
		List<OrderProduct> orderProducts = orderProduct.findAll();
		return orderProducts;
	}
	
}
