package dao;

import java.util.List;

import model.Product;

public class ProductDAO extends AbstractDAO<Product> {
	
	public Product findProduct(int id) {
		ProductDAO product = new ProductDAO();
		return product.findById(id);
	}
	
	public void deleteProduct(int id) {
		ProductDAO product = new ProductDAO();
		product.deleteById(id);
	}
	
	
	public void updateProduct(int id, Product p) {
		ProductDAO product = new ProductDAO();
		product.update(id, p);
	}
	
	public void insertProduct(Product p) {
		ProductDAO product = new ProductDAO();
		product.insert(p);
	}
	
	public List<Product> findAllProduct() {
		ProductDAO product = new ProductDAO();
		List<Product> products = product.findAll();
		return products;
	}

}
