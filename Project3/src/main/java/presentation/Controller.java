package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import bll.ConnectionBLL;
import bll.CustomerBLL;
import bll.OrderCustomerBLL;
import bll.OrderProductBLL;
import bll.ProductBLL;
import model.Customer;
import model.Product;

public class Controller {
	
	private View guiView;
	private ViewOrder guiOrderView;
	CustomerBLL customerBLL;
	ProductBLL productBLL;
	ConnectionBLL connect = new ConnectionBLL();
	OrderCustomerBLL orderCustomerBLL;
	OrderProductBLL orderProductBLL;
	ClientsTable clientsTable;
	
	public Controller(View guiView) {
		this.guiView = guiView;
		
		customerBLL = new CustomerBLL();
		productBLL = new ProductBLL();
		orderCustomerBLL = new OrderCustomerBLL();
		orderProductBLL = new OrderProductBLL();
		clientsTable = new ClientsTable();
		
		
		this.guiView.makeOrderListener(new ViewOrder());
		this.guiView.editCustomerListener(new ViewCustomer());
		this.guiView.editProductListener(new ViewProduct());
		
		
		
		/*if(ViewOrder.wasMade == true) {
			guiOrderView.addToCartButtonListener(new CartListener());
			guiOrderView.orderButtonListener(new OrderListener());
			guiOrderView.refreshProductButtonListener(new RefreshProductListener());	
		}*/
		//guiOrderView.addToCartButtonListener(new CartListener());
		//guiOrderView.orderButtonListener(new OrderListener());
		//guiOrderView.refreshProductButtonListener(new RefreshProductListener());
		
		//this.guiView.addToCartButtonListener(new CartListener());
		//this.guiView.orderButtonListener(new OrderListener());
		//this.guiView.refreshProductButtonListener(new RefreshProductListener());
	}
	
	/*public class CartListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			try {
				Customer customer = customerBLL.findById(guiOrderView.getCustomerRow());
				System.out.println(customer);
				Product product = productBLL.findById(guiOrderView.getProductRow());
				
				int quantity = guiOrderView.getNumberOfItems();
				System.out.println(quantity);
				System.out.println(product);
				
				connect.connectTable(customer, product, quantity);
			}
			catch(Exception ex) {
				ex.printStackTrace();
				//guiView.displayErrorMessage("Error cart!");
			}
		}
	}
	
	class RefreshProductListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			try {
				guiOrderView.refreshProductTable();
			}
			catch(Exception ex) {
				ex.printStackTrace();
				//guiView.displayErrorMessage("Error cart!");
			}
		}
	}
	
	class OrderListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			try {
				Customer customer = customerBLL.findById(guiOrderView.getCustomerRow());
				
				connect.createBill(customer);
			}
			catch(Exception ex) {
				ex.printStackTrace();
				//guiView.displayErrorMessage("Error order!");
			}
		}
	}*/
}
