package presentation;

import java.util.List;

import javax.swing.JTable;

import bll.CustomerBLL;
import model.Customer;

public class ClientsTable extends AbstractTable<Customer>{
	
	private CustomerBLL customerBLL;
	private List<Customer> customers;
	
	
	public JTable create(List<Customer> t) {
		ClientsTable  clients = new ClientsTable();
		JTable clientsTable = clients.createTable(t);
		return clientsTable;
	}
	
	public JTable createTable() {
		customerBLL = new CustomerBLL();
		customers = customerBLL.findAll();
		
		return create(customers);
	}
	
}
