package presentation;

import javax.swing.*;

import bll.CustomerBLL;
import bll.ProductBLL;
import model.Customer;
import model.Product;

import java.awt.*;
import java.awt.event.*;

public class ViewCustomer implements ActionListener{
	
	private JFrame customerFrame;
	private JPanel panelFin, panelCustomer;
	private JTable clients;
	private JLabel idLabel, nameLabel, emailLabel, ageLabel, addressLabel;
	private JTextField idTextField, nameTextField, emailTextField, ageTextField, addressTextField;
	private JButton addCustomer, deleteCustomer, editCustomer;
	private JScrollPane clientsPane;
	private ClientsTable clientsT;
	
	public void actionPerformed(ActionEvent e) {
		try { 
			customerFrame = new JFrame("Customer");
			
			panelFin = new JPanel();
			panelCustomer = new JPanel();
			
			idLabel = new JLabel("ID");
			idTextField = new JTextField(5);
			nameLabel = new JLabel("Name");
			nameTextField = new JTextField(5);
			addressLabel = new JLabel("Address");
			addressTextField = new JTextField(5);
			emailLabel = new JLabel("Email");
			emailTextField = new JTextField(5);
			ageLabel = new JLabel("Age");
			ageTextField = new JTextField(5);
			addCustomer = new JButton("Add Customer");
			deleteCustomer = new JButton("Delete Customer");
			editCustomer = new JButton("Edit Customer");
			
			clientsT = new ClientsTable();
			clients = clientsT.createTable();
			clients.setPreferredSize(new Dimension(800, 500));
			clients.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			clientsPane = new JScrollPane(clients);
			clientsPane.setPreferredSize(new Dimension(800, 300));
			
			
			panelFin.add(idLabel);
			panelFin.add(idTextField);
			panelFin.add(nameLabel);
			panelFin.add(nameTextField);
			panelFin.add(addressLabel);
			panelFin.add(addressTextField);
			panelFin.add(emailLabel);
			panelFin.add(emailTextField);
			panelFin.add(ageLabel);
			panelFin.add(ageTextField);
			
			
			
			panelFin.add(addCustomer);
			panelFin.add(deleteCustomer);
			panelFin.add(editCustomer);
			
			panelCustomer.add(clientsPane);
			panelFin.add(panelCustomer);
			
			addCustomer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int idCustomer = Integer.parseInt(idTextField.getText());
						String nameCustomer = nameTextField.getText();
						String addressCustomer = addressTextField.getText();
						String emailCustomer = emailTextField.getText();
						int ageCustomer = Integer.parseInt(ageTextField.getText());
						
						Customer customer = new Customer(idCustomer, nameCustomer, addressCustomer, emailCustomer, ageCustomer);
						
						CustomerBLL customerBLL = new CustomerBLL();
						customerBLL.insert(customer);
					}
					catch(IllegalArgumentException ex) {
						JOptionPane.showMessageDialog(customerFrame, ex.getMessage());
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						JOptionPane.showMessageDialog(customerFrame, "Wrong values entered!");
						//guiView.displayErrorMessage("Error order!");
					}
					refresh();
				}
			});
			
			
			deleteCustomer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int idCustomer = Integer.parseInt(idTextField.getText());
				
						CustomerBLL customerBLL = new CustomerBLL();
						customerBLL.findById(idCustomer);
						customerBLL.deleteById(idCustomer);
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						JOptionPane.showMessageDialog(customerFrame, "Customer does not exist!");
						//guiView.displayErrorMessage("Error order!");
					}
					refresh();
				}
			});
			
			editCustomer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						String nameFinal;
						String addressFinal;
						String emailFinal;
						int ageFinal;
						
						int idCustomer = Integer.parseInt(idTextField.getText());
						
						CustomerBLL customerBLL = new CustomerBLL();
						Customer customerOld = customerBLL.findById(idCustomer);
						
						if(nameTextField.getText().equals("")) {
							nameFinal = customerOld.getCustomerName();
						}
						else {
							nameFinal = nameTextField.getText();
						}
						
						if(addressTextField.getText().equals("")) {
							addressFinal = customerOld.getCustomerAddress();
						}
						else {
							addressFinal = addressTextField.getText();
						}
						
						if(emailTextField.getText().equals("")) {
							emailFinal = customerOld.getCustomerEmail();
						}
						else {
							emailFinal = emailTextField.getText();
						}
						
						if(ageTextField.getText().equals("")) {
							ageFinal = customerOld.getCustomerAge();
						}
						else {
							ageFinal = Integer.parseInt(ageTextField.getText());
						}
						
						
						Customer newCustomer = new Customer(idCustomer, nameFinal, addressFinal, emailFinal, ageFinal);
						
						customerBLL.update(idCustomer, newCustomer);
					}
					catch(Exception ex) {
						JOptionPane.showMessageDialog(customerFrame, "Customer does not exist!");
						//ex.printStackTrace();
						//guiView.displayErrorMessage("Error order!");
					}
					refresh();
				}
			});
			
			customerFrame.add(panelFin);
			customerFrame.setSize(900,400);
			customerFrame.setLocationRelativeTo(null);
			customerFrame.setResizable(false);
			customerFrame.setVisible(true);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			//guiView.displayErrorMessage("Error cart!");
		}
		
	}
	
	private void refresh() {
		
		JTable newTable = clientsT.createTable();
		newTable.setPreferredSize(new Dimension(800, 500));
		newTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane newPane = new JScrollPane(newTable);
		newPane.setPreferredSize(new Dimension(800, 300));
		
		panelCustomer.removeAll();
		panelCustomer.add(newPane);
		panelCustomer.revalidate();
		panelCustomer.repaint();
		
		idTextField.setText("");
		nameTextField.setText("");
		emailTextField.setText("");
		ageTextField.setText("");
		addressTextField.setText("");
		
	}
	
}

