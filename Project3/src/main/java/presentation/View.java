package presentation;

import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class View extends JFrame {
	
	private JPanel panelFin;
	private JButton editCustomerButton, editProductButton, makeOrderButton;
	
	
	public View() {
		
		super("Warehouse");
		setLayout(new GridLayout());
		setSize(200,200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		
		editCustomerButton = new JButton("Edit Customer Table");
		editProductButton = new JButton("Edit Product Table");
		makeOrderButton = new JButton("Make an order");
		
		
		panelFin.add(editCustomerButton);
		panelFin.add(editProductButton);
		panelFin.add(makeOrderButton);
		add(panelFin);
		//pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public void editCustomerListener(ActionListener listenForEditCustomerButton) {
		editCustomerButton.addActionListener(listenForEditCustomerButton);
	}
	
	public void editProductListener(ActionListener listenForEditProductButton) {
		editProductButton.addActionListener(listenForEditProductButton);
	}
	
	public void makeOrderListener(ActionListener listenForMakeOrderButton) {
		makeOrderButton.addActionListener(listenForMakeOrderButton);
	}
	
}


/*public class View extends JFrame{
	
	private JPanel panelFin;
	private JTable clients, products;
	private JLabel labelNumberOfItems;
	private JTextField textNumberOfItems;
	private JButton orderButton, addToCart, backButton, refreshProduct;
	
	public View() {
		
		super("Warehouse");
		setLayout(new GridLayout());
		setSize(850,330);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		
		backButton = new JButton("Back");
		
		ClientsTable clientsT = new ClientsTable();
		clients = clientsT.createTable();
		clients.setPreferredSize(new Dimension(800, 100));
		clients.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane clientsPane = new JScrollPane(clients);
		clientsPane.setPreferredSize(new Dimension(800, 100));
		
		ProductsTable productsT = new ProductsTable();
		products = productsT.createTable();
		products.setPreferredSize(new Dimension(800, 100));
		products.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane productPane = new JScrollPane(products);
		productPane.setPreferredSize(new Dimension(800, 100));
		
		labelNumberOfItems = new JLabel("Insert number of items");
		textNumberOfItems = new JTextField(5);
		
		addToCart = new JButton("Add To Cart");
		refreshProduct = new JButton("Refresh");
		orderButton = new JButton("Create Bill");
		
		panelFin.add(backButton);
		panelFin.add(clientsPane);
		panelFin.add(productPane);
		panelFin.add(labelNumberOfItems);
		panelFin.add(textNumberOfItems);
		panelFin.add(addToCart);
		panelFin.add(refreshProduct);
		panelFin.add(orderButton);
		
		clients.setVisible(true);
		add(panelFin);
		setVisible(true);
		
	}
	
	public int getNumberOfItems() {
		return Integer.parseInt(textNumberOfItems.getText());
	}
	
	public int getCustomerRow() {
		System.out.println((Integer) clients.getValueAt(clients.getSelectedRow(), 0));
		return (Integer) clients.getValueAt(clients.getSelectedRow(), 0);
	}
	
	public int getProductRow() {
		System.out.println((Integer) products.getValueAt(products.getSelectedRow(), 0));
		return (Integer) products.getValueAt(products.getSelectedRow(), 0);
	}
	
	public void refreshProductTable() {
		products.repaint();
	}
	
	public void orderButtonListener(ActionListener listenForOrderButton) {
		orderButton.addActionListener(listenForOrderButton);
	}
	
	public void addToCartButtonListener(ActionListener listenForCartButton) {
		addToCart.addActionListener(listenForCartButton);
	}
	
	public void refreshProductButtonListener(ActionListener refreshButton) {
		addToCart.addActionListener(refreshButton);
	}
	
	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
}*/
