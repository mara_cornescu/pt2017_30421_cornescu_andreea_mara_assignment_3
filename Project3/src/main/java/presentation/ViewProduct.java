package presentation;

import javax.swing.*;

import bll.ConnectionBLL;
import bll.CustomerBLL;
import bll.ProductBLL;
import model.Customer;
import model.Product;

import java.awt.*;
import java.awt.event.*;

public class ViewProduct implements ActionListener{
	
	private JFrame productFrame;
	private JPanel panelFin, panelProducts;
	private JTable products;
	private JLabel idLabel, nameLabel, priceLabel, stockLabel;
	private JTextField idTextField, nameTextField, priceTextField, stockTextField;
	private JButton addProduct, deleteProduct, editProduct;
	private JScrollPane productPane;
	private ProductsTable productsT;
	
	public void actionPerformed(ActionEvent e) {
		try { 
			productFrame = new JFrame("Product");
			
			panelFin = new JPanel();
			panelProducts = new JPanel();
			
			idLabel = new JLabel("ID");
			idTextField = new JTextField(5);
			nameLabel = new JLabel("Name");
			nameTextField = new JTextField(5);
			priceLabel = new JLabel("Price");
			priceTextField = new JTextField(5);
			stockLabel = new JLabel("Stock");
			stockTextField = new JTextField(5);
			addProduct = new JButton("Add Product");
			deleteProduct = new JButton("Delete Product");
			editProduct = new JButton("Edit Product");
			
		    productsT = new ProductsTable();
			products = productsT.createTable();
			products.setPreferredSize(new Dimension(800, 500));
			products.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			productPane = new JScrollPane(products);
			productPane.setPreferredSize(new Dimension(800, 300));
			
			panelFin.add(idLabel);
			panelFin.add(idTextField);
			panelFin.add(nameLabel);
			panelFin.add(nameTextField);
			panelFin.add(priceLabel);
			panelFin.add(priceTextField);
			panelFin.add(stockLabel);
			panelFin.add(stockTextField);
			
			
			
			panelFin.add(addProduct);
			panelFin.add(deleteProduct);
			panelFin.add(editProduct);
			
			panelProducts.add(productPane);
			panelFin.add(panelProducts);
			
			addProduct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int idProduct = Integer.parseInt(idTextField.getText());
						String nameProduct = nameTextField.getText();
						int priceProduct = Integer.parseInt(priceTextField.getText());
						int stockProduct = Integer.parseInt(stockTextField.getText());
						
						Product product = new Product(idProduct, nameProduct, priceProduct, stockProduct);
						
						ProductBLL productBLL = new ProductBLL();
						productBLL.insert(product);
						
					}
					catch(IllegalArgumentException ex) {
						JOptionPane.showMessageDialog(productFrame, ex.getMessage());
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						//guiView.displayErrorMessage("Error order!")
						JOptionPane.showMessageDialog(productFrame, "Wrong values entered!");
					}
					
					refresh();
				}
			});
			
			deleteProduct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						int idProduct = Integer.parseInt(idTextField.getText());
				
						ProductBLL productBLL = new ProductBLL();
						productBLL.findById(idProduct);
						productBLL.deleteById(idProduct);
					}
					catch(IllegalArgumentException ex) {
						JOptionPane.showMessageDialog(productFrame, ex.getMessage());
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						//guiView.displayErrorMessage("Error order!");
						JOptionPane.showMessageDialog(productFrame, "Product does not exist!");
					}
					
					refresh();					
				}
			});
			
			editProduct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						String nameFinal;
						int priceFinal;
						int stockFinal;
						
						int idProduct = Integer.parseInt(idTextField.getText());
						
						ProductBLL productBLL = new ProductBLL();
						Product oldProduct = productBLL.findById(idProduct);
						
						if(nameTextField.getText().equals("")) {
							nameFinal = oldProduct.getProductName();
						}
						else {
							nameFinal = nameTextField.getText();
						}
						
						if(priceTextField.getText().equals("")) {
							priceFinal = oldProduct.getProductPrice();
						}
						else {
							priceFinal = Integer.parseInt(priceTextField.getText());
						}
						
						if(stockTextField.getText().equals("")) {
							stockFinal = oldProduct.getProductStock();
						}
						else {
							stockFinal = Integer.parseInt(stockTextField.getText());
						}
						
						Product newProduct = new Product(idProduct, nameFinal, priceFinal, stockFinal);
						
						productBLL.update(idProduct, newProduct);
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						JOptionPane.showMessageDialog(productFrame, "Product does not exist!");
						//guiView.displayErrorMessage("Error order!");
					}
					refresh();
				}
			});
			
			productFrame.add(panelFin);
			productFrame.setSize(850,400);
			productFrame.setLocationRelativeTo(null);
			productFrame.setResizable(false);
			productFrame.setVisible(true);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			//guiView.displayErrorMessage("Error cart!");
		}
		
	}
	
	private void refresh() {
		
		JTable newTable = productsT.createTable();
		newTable.setPreferredSize(new Dimension(800, 500));
		newTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane newPane = new JScrollPane(newTable);
		newPane.setPreferredSize(new Dimension(800, 300));
		
		panelProducts.removeAll();
		panelProducts.add(newPane);
		panelProducts.revalidate();
		panelProducts.repaint();
		
		idTextField.setText("");
		nameTextField.setText("");
		priceTextField.setText("");
		stockTextField.setText("");
		
	}
}

