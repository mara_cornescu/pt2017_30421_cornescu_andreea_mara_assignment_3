package presentation;

import javax.swing.*;

import bll.ConnectionBLL;
import bll.CustomerBLL;
import bll.ProductBLL;
import model.Customer;
import model.Product;

import java.awt.*;
import java.awt.event.*;

public class ViewOrder implements ActionListener{
	
	private JFrame orderFrame;
	private JPanel panelFin, panelProducts;
	private JTable clients, products;
	private JLabel labelNumberOfItems;
	private JTextField textNumberOfItems;
	private JButton orderButton, addToCart, refreshProduct;
	private JScrollPane clientsPane, productPane;
	private ClientsTable clientsT;
	private ProductsTable productsT;
	
	boolean nowNewTable = false;
	JTable newTable;
	//CustomerBLL customerBLL;
	//ProductBLL productBLL;
	//ConnectionBLL connect;
	
	public void actionPerformed(ActionEvent e) {
		try { 
			
			
			//connect = new ConnectionBLL();
			//productBLL = new ProductBLL();
			//customerBLL = new CustomerBLL();
			//JFrame orderFrame = new JFrame("Order");
			orderFrame = new JFrame("Order");
			panelFin = new JPanel();
			panelProducts = new JPanel();
			
			labelNumberOfItems = new JLabel("Number of items");
			textNumberOfItems = new JTextField(5);
			addToCart = new JButton("Add To Cart");
			refreshProduct = new JButton("Refresh Table");
			orderButton = new JButton("Create Bill");
			
			clientsT = new ClientsTable();
			clients = clientsT.createTable();
			clients.setPreferredSize(new Dimension(800, 500));
			clients.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			clientsPane = new JScrollPane(clients);
			clientsPane.setPreferredSize(new Dimension(800, 200));
			
			productsT = new ProductsTable();
			products = productsT.createTable();
			products.setPreferredSize(new Dimension(800, 500));
			products.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			productPane = new JScrollPane(products);
			productPane.setPreferredSize(new Dimension(800, 200));
			
			panelFin.add(clientsPane);
			panelFin.add(labelNumberOfItems);
			panelFin.add(textNumberOfItems);
			panelFin.add(addToCart);
			panelFin.add(refreshProduct);
			panelFin.add(orderButton);
			
			panelProducts.add(productPane);
			panelFin.add(panelProducts);
			
			addToCart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						CustomerBLL customerBLL = new CustomerBLL();
						ProductBLL productBLL = new ProductBLL();
						ConnectionBLL connect = new ConnectionBLL();
						
						Customer customer = customerBLL.findById((Integer) clients.getValueAt(clients.getSelectedRow(), 0));
						System.out.println(customer);
						
						Product product;
						
						if(nowNewTable == false) {
							 product = productBLL.findById((Integer) products.getValueAt(products.getSelectedRow(), 0));
							 nowNewTable = true;
						} else {
							
							 product = productBLL.findById((Integer) newTable.getValueAt(newTable.getSelectedRow(), 0));
							
						}
						
						int quantity = Integer.parseInt(textNumberOfItems.getText());
						//System.out.println(products.getSelectedRow());
					
						System.out.println(quantity);
						System.out.println(product);
						
						connect.connectTable(customer, product, quantity);
					}
					catch(IllegalArgumentException ex) {
						JOptionPane.showMessageDialog(orderFrame, ex.getMessage());
					}
					catch(Exception ex) {
						//ex.printStackTrace();
						JOptionPane.showMessageDialog(orderFrame, "Wrong values entered!");
						//guiView.displayErrorMessage("Error cart!");
					}
					refresh();
				}
			});
			
			orderButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						CustomerBLL customerBLL = new CustomerBLL();
						ConnectionBLL connect = new ConnectionBLL();
						
						Customer customer = customerBLL.findById((Integer) clients.getValueAt(clients.getSelectedRow(), 0));
						System.out.println(customer);
						connect.createBill(customer);
					}
					catch(Exception ex) {
						ex.printStackTrace();
						//guiView.displayErrorMessage("Error order!");
					}
				}
			});
			
			refreshProduct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {			
					/*products.removeAll();
					products.revalidate();
					products.repaint();
					productPane.removeAll();
					productPane.revalidate();
					productPane.repaint();
					productsPanel.add(productPane);*/
					refresh();
					
				}
			});
			
			orderFrame.add(panelFin);
			orderFrame.setSize(850,500);
			orderFrame.setLocationRelativeTo(null);
			orderFrame.setResizable(false);
			orderFrame.setVisible(true);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			//guiView.displayErrorMessage("Error cart!");
		}
		
	}
	
	private void refresh() {
		products.revalidate();
		products.repaint();
		
		newTable = productsT.createTable();
		newTable.setPreferredSize(new Dimension(800, 500));
		newTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane newPane = new JScrollPane(newTable);
		newPane.setPreferredSize(new Dimension(800, 200));
		
		panelProducts.removeAll();
		panelProducts.add(newPane);
		panelProducts.revalidate();
		panelProducts.repaint();
		
		textNumberOfItems.setText("");
	}

}
