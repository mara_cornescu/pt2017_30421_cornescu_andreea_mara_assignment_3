package presentation;

import java.util.List;

import javax.swing.JTable;

import bll.ProductBLL;
import model.Product;

public class ProductsTable extends AbstractTable<Product>{
	
	ProductBLL productBLL;
	List<Product> products;
	
	public JTable create(List<Product> t) {
		ProductsTable  products = new ProductsTable();
		JTable productsTable = products.createTable(t);
		return productsTable;
	}
	
	public JTable createTable() {
		productBLL = new ProductBLL();
		products = productBLL.findAll();
		
		return create(products);
	}
	
}
